﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneController : MonoBehaviour
{
    private static SceneController _instance = null;
    public static SceneController Instance { 
        get
        {
            if(_instance == null)
            {
                Debug.LogWarning("SceneController didnt exist on load, creating one.");
                GameObject ic = new GameObject("SceneController");
                _instance = ic.AddComponent<SceneController>();
            }
            return _instance;
        }
    }

    InputManager input;

    public GameObject pauseScreen;

    private void Update() 
    {
        if (input.CancelDown)
        {
            if (pauseScreen.activeSelf)
            {
                UnpauseGame();
            }
            else
            {
                PauseGame();
            }
        }
    }

    public void PauseGame()
    {
        Time.timeScale = 0f;
        if (pauseScreen) pauseScreen.SetActive(true);
    }

    public void UnpauseGame()
    {
        Time.timeScale = 1f;
        if (pauseScreen) pauseScreen.SetActive(false);
    }

    public void GoToMainMenu()
    {

    }

    public void ExitGame()
    {
        Application.Quit();   
    }

    private void Start() 
    {
        input = InputManager.Instance;
    }

    private void Awake() 
    {
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        } else {
            _instance = this;
        }
    }
}
