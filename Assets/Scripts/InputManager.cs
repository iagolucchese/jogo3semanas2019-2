﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputManager : MonoBehaviour //Singleton
{
    private static InputManager _instance = null;
    public static InputManager Instance { 
        get
        {
            if(_instance == null)
            {
                Debug.LogWarning("InputManager didnt exist on load, creating one.");
                GameObject ic = new GameObject("InputManager");
                _instance = ic.AddComponent<InputManager>();
            }

            return _instance;
        }
    }

    public bool UsingKeyboard = false;

    [Header("Player")]
    public float HorizontalInput = 0f;
    public float VerticalInput = 0f;
    public bool Horizontal;
    public bool Vertical;

    public bool Fire1Down = false;
    public bool Fire1 = false;
    public bool Fire2Down = false;
    public bool Fire2 = false;

    [Header("Camera")]
    public float CameraHorizontalInput = 0f;
    public float CameraVerticalInput = 0f;
    public bool CameraZoomButtonDown = false;
    public bool CameraCenterButtonDown = false;

    [Header("UI")]
    public bool SubmitDown = false;
    public bool CancelDown = false;

    void GrabPlayerInputs()
    {
        //assumindo que o player usa KB+M ou controle de XBOX 

        //eixos de movimento do player
        //horizontal
        if (Input.GetAxis("HorizontalKB") != 0)
        {
            HorizontalInput = Input.GetAxis("HorizontalKB");
            UsingKeyboard = true;
            Horizontal = true;
        } 
        else if (Input.GetAxis("HorizontalCon") != 0)
        {
            HorizontalInput = Input.GetAxis("HorizontalCon");
            UsingKeyboard = false;
            Horizontal = true;
        }
        else
        {
            HorizontalInput = 0f;
            Horizontal = false;
        }
        

        //vertical
        if (Input.GetAxis("VerticalKB") != 0)
        {
            VerticalInput = Input.GetAxis("VerticalKB");
            UsingKeyboard = true;
            Vertical = true;
        } 
        else if (Input.GetAxis("VerticalCon") != 0)
        {
            VerticalInput = Input.GetAxis("VerticalCon");
            UsingKeyboard = false;
            Vertical = true;
        }
        else
        {
            VerticalInput = 0f;
            Vertical = false;
        }        

        //botões de ação
        Fire1Down = GetButtonDownInput("Fire1KB", "Fire1Con");
        Fire2Down = GetButtonDownInput("Fire2KB", "Fire2Con");
    }

    void GrabCameraInputs()
    {
        CameraHorizontalInput = GetAxisInput("Mouse X", "CameraHorizontalCon");
        CameraVerticalInput = GetAxisInput("Mouse Y", "CameraVerticalCon");

        //botões de camera
        CameraZoomButtonDown = GetButtonDownInput("CameraZoomKB", "CameraZoomCon");
        CameraCenterButtonDown = GetButtonDownInput("CameraCenterKB", "CameraCenterCon");
    }

    void GrabUIInputs()
    {
        SubmitDown = GetButtonDownInput("SubmitKB", "SubmitCon");
        CancelDown = GetButtonDownInput("CancelKB", "CancelCon");

    }

    float GetAxisInput(string KBAxisName, string ConAxisName, out bool inputBool)
    {
        float output;
        if (Input.GetAxis(KBAxisName) != 0)
        {
            output = Input.GetAxis(KBAxisName);
            UsingKeyboard = true;
            inputBool = true;
        } 
        else if (Input.GetAxis(ConAxisName) != 0)
        {
            output = Input.GetAxis(ConAxisName);
            UsingKeyboard = false;
            inputBool = true;
        }
        else
        {
            output = 0f;
            inputBool = false;
        }
        return output;
    }

    float GetAxisInput(string KBAxisName, string ConAxisName)
    {
        float output;
        if (Input.GetAxis(KBAxisName) != 0)
        {
            output = Input.GetAxis(KBAxisName);
            UsingKeyboard = true;
        } 
        else if (Input.GetAxis(ConAxisName) != 0)
        {
            output = Input.GetAxis(ConAxisName);
            UsingKeyboard = false;
        }
        else
        {
            output = 0f;
        }
        return output;
    }

    bool GetButtonDownInput(string KBButtonName, string ConButtonName)
    {
        bool output;
        if (Input.GetButtonDown(KBButtonName))
        {
            output = true;
            UsingKeyboard = true;
        } 
        else if (Input.GetButtonDown(ConButtonName))
        {
            output = true;
            UsingKeyboard = false;
        }
        else
        {
            output = false;
        }
        return output;
    }

    bool GetButtonInput(string KBButtonName, string ConButtonName)
    {
        bool output;
        if (Input.GetButton(KBButtonName))
        {
            output = true;
            UsingKeyboard = true;
        } 
        else if (Input.GetButton(ConButtonName))
        {
            output = true;
            UsingKeyboard = false;
        }
        else
        {
            output = false;
        }
        return output;
    }

    void Update()
    {
        GrabPlayerInputs();
        GrabCameraInputs();
        GrabUIInputs();
    }

    void Awake()
    {
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        } else {
            _instance = this;
        }
    }
}
